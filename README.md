# Vanilla Weather Application

The application shows a current weather info, todays and 6-day forecast. Application was created with such technologies as Bootstrap, AJAX and OpenWeather API. OpenWeather API provides access to current weather data for any location on Earth including over 200,000 cities. The data is frequently updated based on the global and local weather models, satellites, radars and a vast network of weather stations.

## Usage
To use application, just click here - [ Weather App 🌥](https://darias-weather-in-your-city.netlify.app).

Enter city you live or press button "My location" to see current weather info and 6-days forecast.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

1. Before you begin, choose the folder you want to work in. 
2. Clone a repository. The files from the remote repository will be downloaded to your computer.
```
clone git@gitlab.com:darnahorna/weather-application.git
```
3. Now you can open the folder in any of IDE for editing code.

## Author

Daria Nahorna 

## Acknowledgments
This project is designed for educational purposes. The project developed the layout, design and functionality of the application. The app shows current weather information now and also gives a weather forecast for 6 days
