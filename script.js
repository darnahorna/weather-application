let apiKey = "69967927e28d8590b78f96c348d2023b";
let month = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];
let days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
//function to check time format and add 0 (eg not 8:4, but 08:04)
function checkTime(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

//set date
function setDate(timestamp) {
  let now = new Date(timestamp);
  //create Data using Time of data calculation, unix, UTC
  //special type for Date

  //reffers to array of days an
  let currentDate = now.getDate();
  let currentMonth = month[now.getMonth()];
  let currentHours = now.getHours();
  let currentMinutes = now.getMinutes();
  currentHours = checkTime(currentHours);
  //call function to add 0 if it is needed
  currentMinutes = checkTime(currentMinutes);

  return `${currentDate} ${currentMonth} ${currentHours}:${currentMinutes}`;
}

function toCelsius(temperature) {
  //formula from fahreinheit to celsius
  return ((temperature - 32) * 5) / 9;
}

function toFahrenheit(temperature) {
  //formula from celsius to fahreinheit
  return (temperature * 9) / 5 + 32;
}

function changeTemperatureToCelsius(event) {
  //function to change temperature in html
  event.preventDefault();
  let currentTemperature = document.querySelector("#current-temperature");
  currentTemperature.innerHTML = Math.round(
    toCelsius(currentTemperature.textContent)
  );

  fahrenheitData.classList.remove("selected"); //marker for units
  celsiusData.classList.add("selected");
}

function changeTemperatureToFahrenheit(event) {
  event.preventDefault();
  let currentTemperature = document.querySelector("#current-temperature");

  currentTemperature.innerHTML = Math.round(
    toFahrenheit(currentTemperature.textContent)
  );

  celsiusData.classList.remove("selected"); //marker to make unit choice impossible for celsuius
  fahrenheitData.classList.add("selected"); //activet class for Fahrenhei
}
function formatDay(timestamp) {
  //format date for forecast
  let date = new Date(timestamp * 1000);

  let day = date.getDay();

  return `${days[day]}, ${date.getDate()} ${month[date.getMonth()]}`;
}

function displayForecast(response) {
  let forecast = response.data.daily; //special API section for forecast

  let forecastElement = document.querySelector("#forecast");
  let probabilityPrecipitationElement = document.querySelector(
    "#probability-precipitation"
  ); //special variable for precipitation because pr. is saved in daily section

  let forecastHTML = `<div class="row">`; //html injection
  forecast.forEach(function (forecastDay, index) {
    if (index < 6) {
      //show only 6 days forecast
      forecastHTML =
        forecastHTML +
        `
          <div class="col-2 weather-unit">
            <div class="weather-forecast-date">${formatDay(forecastDay.dt)}
            </div>
            <img
              src="http://openweathermap.org/img/wn/${
                forecastDay.weather[0].icon
              }@2x.png"
              alt=""
              class="weather-forecast-icon"
            />
            <div class="weather-forecast-temperature">
              <span class="weather-forecast-temperature-max" id ="forecast-max-temp"> ${Math.round(
                forecastDay.temp.max
              )}° </span>
              <span class="weather-forecast-temperature-min"> ${Math.round(
                forecastDay.temp.min
              )}° </span>
            </div>
          </div>`;
    }
  });
  probabilityPrecipitationElement.innerHTML = `${forecast[0].pop} %`; //set precipitation
  forecastHTML = forecastHTML + `</div>`; //close html injection
  forecastElement.innerHTML = forecastHTML; //set html forecast
}

function getForecast(coordinates) {
  let lat = coordinates.lat;
  let lon = coordinates.lon;

  let apiUrl = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=${apiKey}&units=metric`;
  axios.get(apiUrl).then(displayForecast);
}

function showTemperature(response) {
  let currentTemperature = document.querySelector("#current-temperature");
  let weatherDescription = document.querySelector("#weather-description");
  let currentCity = document.querySelector("#current-city");
  let currentHumidity = document.querySelector("#humidity");
  let currentPressure = document.querySelector("#pressure");
  let currentWind = document.querySelector("#wind");
  let lastUpdate = document.querySelector("#current-date");
  let iconWeather = document.querySelector("#weather-icon");

  let temperature = response.data.main.temp;
  temperature = Math.round(temperature);
  let description = response.data.weather[0].description;
  let city = response.data.name;
  let country = response.data.sys.country;
  let humidity = response.data.main.humidity;
  let wind = response.data.wind.speed;
  let pressure = response.data.main.pressure;
  let timestamp = response.data.dt * 1000;
  let iconLink = `http://openweathermap.org/img/wn/${response.data.weather[0].icon}@2x.png`;

  lastUpdate.innerHTML = setDate(timestamp);
  //Time of data calculation, unix, UTC
  currentTemperature.innerHTML = temperature;
  weatherDescription.innerHTML = description;
  currentCity.innerHTML = `${city}, ${country}`;
  currentWind.innerHTML = `${wind} m/sec`;
  currentHumidity.innerHTML = `${humidity} %`;
  currentPressure.innerHTML = `${pressure} hPa`;
  iconWeather.setAttribute("src", iconLink);
  iconWeather.setAttribute("alt", description);
  getForecast(response.data.coord);
}

function handlePosition(position) {
  let lat = position.coords.latitude;
  let lon = position.coords.longitude;

  let apiUrl = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&appid=${apiKey}`;
  axios.get(apiUrl).then(showTemperature);
}
function searchCity(city) {
  let apiUrl = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`;

  axios.get(apiUrl).then(showTemperature);
}

function inputCity(event) {
  //function to change html content in particular areas (city name)
  event.preventDefault();
  let cityInput = document.querySelector("#search-console"); //take data from search input

  searchCity(cityInput.value);
}

function setUserLocationWeather() {
  navigator.geolocation.getCurrentPosition(handlePosition);
}

let celsiusData = document.querySelector("#convert-to-celsius");

celsiusData.addEventListener("click", changeTemperatureToCelsius);

let fahrenheitData = document.querySelector("#convert-to-fahrenheit");

fahrenheitData.addEventListener("click", changeTemperatureToFahrenheit);

let userLocation = document.querySelector("#my-location");
userLocation.addEventListener("click", setUserLocationWeather);

let searchInput = document.querySelector("#search-city");
searchInput.addEventListener("submit", inputCity);

searchCity("Kyiv");
